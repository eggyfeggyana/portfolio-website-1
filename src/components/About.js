import React from "react";

const About = () => {
  return (
    <div name="About" className="h-screen w-full bg-black text-white">
      <div className="max-w-screen-lg p-4 mx-auto flex-col justify-center w-full h-full">
        <div className="pb-8">
          <p className="text-4xl font-bold inline border-b-4 border-r-gray-500 mt-20">
            About
          </p>
        </div>
        <p className="text-xl mt-15 ">
          Greetings! I'm Eggy Feggyana Kusmayadi, and I'm delighted to welcome
          you to my world of creativity and aspirations. As someone who thrives
          on the thrill of the unknown, I've embarked on a journey to harness
          the power of imagination and innovation. While my journey might not
          boast a long list of past projects, it's very fresh that fuels my
          drive. I believe that every new project is an opportunity to paint a
          fresh canvas, to bring forth novel ideas, and to push the boundaries
          of creativity. Armed with a strong foundation in the principles of
          design and an unquenchable thirst for learning, I'm eagerly taking
          those initial steps towards realizing my potential. My approach to
          projects is one of a curious explorer. I dive deep into research,
          immersing myself in every detail to truly understand the essence of
          the task at hand. I seek inspiration from the world around me – from
          art and culture to nature's intricate designs. This fusion of
          inspiration and dedication allows me to craft solutions that resonate
          on multiple levels.
        </p>
        <br />
        <p className="text-xl">
          What sets me apart is not just my technical skills, but my unyielding
          determination to turn challenges into stepping stones. My adaptability
          and openness to feedback enable me to pivot, refine, and enhance my
          work, ensuring that each project is a masterpiece in its own right.
          Beyond the confines of projects, I find joy in collaboration and
          connection. I'm eager to connect with fellow enthusiasts, mentors, and
          potential collaborators who share my passion for pushing creative
          boundaries. Thank you for taking the time to get to know me better.
          Join me as I embrace the exhilarating path of growth, discovery, and
          creative accomplishment. Here's to a journey that promises not just
          projects, but a legacy of innovation and inspiration. warm regards,
          Eggy Feggyana Kusmayadi.
        </p>
        <br />
        <p className="text-xl">warm regards, Eggy Feggyana Kusmayadi.</p>
      </div>
    </div>
  );
};

export default About;
