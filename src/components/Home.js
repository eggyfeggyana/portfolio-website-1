import React from "react";
import { MdOutlineKeyboardArrowRight } from "react-icons/md";
import ProfilePicture from "./assets/image.png";
import TypeIt from "typeit-react";
import { Link } from "react-scroll";

const Home = () => {
  return (
    <div name="Home" className="h-screen w-full bg-black">
      <div className="max-w-screen-lg mx-auto flex flex-col items-center justify-center h-full px-4 md:flex-row">
        <div className="flex flex-col justify-center h-full">
          <TypeIt className="text-4xl sm:text-7xl font-bold text-white">
            Im Front End Developer
          </TypeIt>
          <p className="text-gray-500 py-4 max-w-md">
            Welcome to My Portfolio! Explore my journey, passion, and creativity
            showcased here. Let's connect and inspire together. [ Eggy Feggyana
            Kusmayadi ]
          </p>
          <Link
            to="Portfolio"
            smooth
            duration={500}
            className="text-white w-fit px-6 py-3 my-2 flex items-center rounded-md bg-gradient-to-r from-cyan-500 to-blue-500 transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-indigo-500 duration-300 shadow-lg shadow-cyan-500/50 ..."
          >
            Portfolio
            <span className="hover:rotate-90 duration-300">
              <MdOutlineKeyboardArrowRight size={25} className="ml-1" />
            </span>
          </Link>
        </div>
        <div className="text-white w-fit px-6 py-3 my-2 flex items-center rounded-md">
          <img
            src={ProfilePicture}
            alt="MyProfile"
            className="rounded-2xl mx-auto w-2/3"
          ></img>
        </div>
      </div>
    </div>
  );
};

export default Home;
