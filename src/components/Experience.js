import React from "react";
import html from "./assets/html-icon.png";
import css from "./assets/css-icon.png";
import python from "./assets/python-programming-language-icon.png";
import redux from "./assets/redux-icon.png";
import react from "./assets/react-js-icon.png";
import tailwind from "./assets/tailwind-css-icon.png";
import gitlab from "./assets/gitlab-icon.png";
import yarn from "./assets/yarn-package-manager-icon.png";
import vscode from "./assets/visual-studio-code-icon.png";
import firebase from "./assets/google-firebase-icon.png";
import node from "./assets/node.png";

const Experience = () => {
  const techs = [
    {
      id: 1,
      src: html,
      tittle: "HTML",
      style: "shadow-orange-500",
    },
    {
      id: 2,
      src: css,
      tittle: "Css",
      style: "shadow-blue-500",
    },
    {
      id: 3,
      src: vscode,
      tittle: "Visual Studio Code ",
      style: "shadow-blue-500",
    },
    {
      id: 4,
      src: python,
      tittle: "Python",
      style: "shadow-yellow-500",
    },
    {
      id: 5,
      src: redux,
      tittle: "Redux",
      style: "shadow-purple-500",
    },
    {
      id: 6,
      src: react,
      tittle: "REACTJS/NATIVE",
      style: "shadow-blue-500",
    },
    {
      id: 7,
      src: tailwind,
      tittle: "Tailwind",
      style: "shadow-sky-500",
    },
    {
      id: 8,
      src: gitlab,
      tittle: "Gitlab",
      style: "shadow-orange-500",
    },
    {
      id: 9,
      src: yarn,
      tittle: "Yarn",
      style: "shadow-sky-500",
    },
    {
      id: 10,
      src: firebase,
      tittle: "Firebase",
      style: "shadow-yellow-500",
    },
    {
      id: 11,
      src: node,
      tittle: "NodeJS",
      style: "shadow-green-500",
    },
  ];

  return (
    <div name="Experience" className="bg-black w-full h-screen">
      <div className="max-w-screen-lg mx-auto p-4 flex flex-col justify-center w-full h-full text-white">
        <div>
          <p className="text-4xl font-bold border-b-4 border-gray-500 p-2 inline">
            Experience
          </p>
          <p className="py-6">These are the technologies i've worked with</p>
        </div>

        <div className="w-full grid grid-cols-2 sm:grid-cols-3 gap-8 text-center py-8 px-12 sm:px-0">
          {techs.map(({ id, src, tittle, style }) => (
            <div
              key={id}
              className={`shadow-md hover:scale-105 duration-500 py-2 rounded-lg ${style}`}
            >
              <img src={src} alt="" className="w-20 mx-auto" />
              <p className="mt-4">{tittle}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Experience;
