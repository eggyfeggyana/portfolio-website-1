import React from "react";
import maintenance from "./assets/—Pngtree—under construction with triangle shape_8811959.png";

const Portfolio = () => {
  return (
    <div name="Portfolio" className="h-screen w-full bg-black text-white">
      <div className="max-w-screen-lg p-4 ml-40 flex flex-col justify-center w-full h-full">
        <div className="pb-9">
          <p className="text-4xl font-bold inline border-b-4 border-r-gray-500 mt20">
            Portfolio
          </p>

          <img src={maintenance} alt="" className="w-150 m-auto" />
        </div>
        <p className="text-xl mt-20 "></p>
      </div>
    </div>
  );
};

export default Portfolio;
